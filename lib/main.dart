import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  String title = "Card";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(title),
        ),
        body: Center(
          heightFactor: 1,
          widthFactor: 1,
          child: Card(
            margin: EdgeInsets.all(8),
            child: Column(
              children: <Widget>[
                Expanded(
                  flex: 10,
                  child: Stack(
                    alignment: Alignment.bottomCenter,
                    children: <Widget>[
                      _buildPageView(),
                      _buildDataSection(),
                      _buildTagSection(),
                    ],
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: _buildCTABar(),
                )
              ],
            ),
          ),
        ));
  }

  Positioned _buildTagSection() {
    return Positioned(
                      top: 0,
                      left: 0,
                      child: ConstrainedBox(
                        constraints:
                            BoxConstraints(maxHeight: 36, maxWidth: 124),
                        child: Container(
                          color: Colors.red[200],
                        ),
                      ),
                    );
  }

  ConstrainedBox _buildDataSection() {
    return ConstrainedBox(
                      constraints: BoxConstraints(maxHeight: 124),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            flex: 5,
                            child: Container(
                              color: Colors.purple[100],
                            ),
                          ),
                          Expanded(
                            flex: 1,
                            child: Container(
                              color: Colors.purple[300],
                            ),
                          )
                        ],
                      ),
                    );
  }

  Container _buildCTABar() {
    return Container(
                  color: Colors.blue,
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        flex: 3,
                        child: Container(
                          color: Colors.green[100],
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Container(
                          color: Colors.green[300],
                        ),
                      )
                    ],
                  ),
                );
  }

  Widget _buildPageView() {
    return PageView(
      children: [
        _buildPage(index: 1, color: Colors.green),
        _buildPage(index: 2, color: Colors.blue),
        _buildPage(index: 3, color: Colors.indigo),
        _buildPage(index: 4, color: Colors.red),
      ],
    );
  }

  Widget _buildPage({int index, Color color}) {
    return Container(
      alignment: AlignmentDirectional.center,
      color: color,
      child: Text(
        '$index',
        style: TextStyle(fontSize: 132.0, color: Colors.white),
      ),
    );
  }
}
